Shoes.app do

  background black

  button("Generate QR Code") {
    system("ruby gencode.rb")
  }

  button("Open QR Code") {
    # Insert directory for qr_code.png
  }

  button("Embed QR Code") {
    # Example image, just use your own .jpg image.
    system("steghide embed -cf petrat.jpg -ef qrcode.png")
  }

  button("Exit QR Reader") {
    Shoes.quit
  }

end