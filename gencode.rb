require "rqrcode_png"

print "Message: "
m = gets.chomp

qr = RQRCode::QRCode.new( m, :size => 4, :level => :h )
png = qr.to_img												# returns an instance of ChunkyPNG
png.resize(400, 400).save("qrcode.png")

puts "Generated QR code..."